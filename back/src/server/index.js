import express from "express";
import { PORT } from "./server.config";
import middleware from '../lib/unfound-middleware'
import MongoDB from '../lib/unfound-api-database/MongoDB/index';
import routes from '../lib/unfound-api/index';

//app is intialized here
const app = express();

//all the middleware of the app are imported here
middleware(app);

//all the routes are defined here
routes(app);

// server is started here
app.listen(PORT, (err) => {
  if (err) {
    console.error(err);
  }
  console.log(`Server is started @${PORT}`)
});