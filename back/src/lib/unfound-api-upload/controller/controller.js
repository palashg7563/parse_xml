import fs from "fs";
import { xml2js } from "xml-js";
import { savejson, get, addvehicle, getvehicles } from "../../unfound-api-database/MongoDB/vehicle"

//function for uploading the file to the req.file
export const uploads = async (req, res) => {
  try {
    res.status(200).json({
      statusCode: 200,
      message: "Success",
      payload: {
        filename: req.file.filename
      }
    });
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
};


//convert the xml file in json
export const json = async (req, res) => {
  try {
    const { filename } = req.body;
    console.log(filename);

    const xml = await fs.readFileSync(`./upload/${filename}`, { encoding: "utf-8" });
    const json1 = xml2js(xml);

    const ob = {};

    ob.no = json1.elements[0].elements.length;
    ob.date = Date.now();
    for (let i = 0; i < json1.elements[0].elements.length; i++) {
      ob[`vehicle_${i}`] = json1.elements[0].elements[i].elements;
      ob[`vehicle_${i}`].id = ob[`vehicle_${i}`][0].elements[0].text;
      ob[`vehicle_${i}`].frame = ob[`vehicle_${i}`][1].elements[0].elements[0].text
      ob[`vehicle_${i}`].wheel = ob[`vehicle_${i}`][2].elements;
      ob[`vehicle_${i}`].powertrain = ob[`vehicle_${i}`][3].elements[0].name
      ob[`vehicle_${i}`].wheel_count = ob[`vehicle_${i}`][2].elements.length
    }
    const wheels = [];
    for (let i = 0; i < json1.elements[0].elements.length; i++) {
      for (let j = 0; j < ob[`vehicle_${i}`].wheel_count; j++) {
        const position = ob[`vehicle_${i}`].wheel[`${j}`].elements[`${0}`].elements[`${0}`].text;
        const material = ob[`vehicle_${i}`].wheel[`${j}`].elements[`${1}`].elements[`${0}`].text;
        wheels.push({ material, position });
      }
    }
    let start = 0;
    let end = 0;
    for (let i = 0; i < ob.no; i++) {
      ob[`vehicle_${i}`].wheel = wheels.slice(start, end + ob[`vehicle_${i}`].wheel_count)
      end = ob[`vehicle_${i}`].wheel_count;
      start = ob[`vehicle_${i}`].wheel_count;
      // ob[`vehicle_${i}`].wheel
    }


    for (let i = 0; i < ob.no; i++) {
      let material_plastic_count = 0;
      let material_metal_count = 0;
      let plasticstring = "";
      let metalstring = "";
      for (let j = 0; j < ob[`vehicle_${i}`].wheel.length; j++) {
        if (ob[`vehicle_${i}`].wheel[`${j}`].material === "plastic") {
          material_plastic_count++;
          plasticstring = plasticstring + " " + ob[`vehicle_${i}`].wheel[`${j}`].position
        } else {
          material_metal_count++;
          metalstring = metalstring + " " + ob[`vehicle_${i}`].wheel[`${j}`].position
        }
        ob[`vehicle_${i}`].wheel[`${j}`] = `metal have ${material_metal_count} with ${metalstring} position and plastic have ${material_plastic_count} with ${plasticstring} position`
      }
    }

    for (let i = 0; i < ob.no; i++) {
      ob[`vehicle_${i}`].wheel = ob[`vehicle_${i}`].wheel[ob[`vehicle_${i}`].wheel.length - 1];
    }

    const vehicles = [];
    for (let i = 0; i < ob.no; i++) {
      const res = await savejson({
        id: ob[`vehicle_${i}`]["id"],
        frame: ob[`vehicle_${i}`]["frame"],
        powertrain: ob[`vehicle_${i}`]["powertrain"],
        wheel: ob[`vehicle_${i}`]["wheel"],
        powertrain: ob[`vehicle_${i}`]['powertrain']
      });
      vehicles.push(res);
    }

    const ids = vehicles.map(i => i.payload);
    const response = await addvehicle({ id: ids });

    res.status(200).json({
      statusCode: 200,
      message: "Success",
      payload: {
        vehicleId: response.payload,
        count: ob.no
      }
    });

  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    });
  }
}


//this function is used to get the xml file via fs module
export const getxml = async () => {
  try {
    const { filename } = req.body;

    const xml = await fs.readFileSync(`./upload/${filename}`, { encoding: "utf-8" });

    res.status(200).json({
      statusCode: 200,
      message: "Success",
      payload: xml
    });
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
}

// this function is used to get the vehicles from the vehicle
export const getAllVehicles = async (req, res) => {
  try {
    const { id } = req.body;

    const response = await getvehicles({ id: id });

    if (response.statusCode === 200) {
      res.status(200).json({
        statusCode: 200,
        id: response.id,
        message: response.message,
        payload: response.payload
      })
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
}