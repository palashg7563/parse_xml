import multer from "multer"
import uuid from "uuid/v4"
import path from "path";

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, "./upload");
  },
  filename: (req, file, callback) => {
    const newFileName = `${uuid()}${path.extname(file.originalname)}`;
    callback(null, newFileName);
  }
})

export const upload = multer({ storage })
