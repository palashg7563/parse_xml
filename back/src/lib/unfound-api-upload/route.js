import express from "express";
import { upload } from "./controller/middleware";
import { uploads, json, getxml, getAllVehicles } from "./controller/controller";
const route = express.Router();

route.post("/", upload.single("selectedFile"), uploads);
route.post("/json", json);
route.get("/xml", getxml);
route.post("/vehicles", getAllVehicles)

export default route;