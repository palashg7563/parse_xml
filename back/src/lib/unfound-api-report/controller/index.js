//this file contain the controller section of the report section

import { save, get, getAll } from "../../unfound-api-database/MongoDB/report";

export const saves = async (req, res) => {
  try {
    const { id } = req.body;

    const response = await save({ id: id });

    if (response.statusCode === 201) {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        message: response.statusCode,
        payload: response.payload
      })
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
}


// this function is used to get the report from the db
export const gets = async (req, res) => {
  try {
    const { id } = req.body;

    const response = await get({ id: id });

    if (response.statusCode >= 400) {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        message: response.message,
        payload: response.payload
      });
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
}

export const getAlls = async (req, res) => {
  try {
    const res1 = await getAll();

    if (res1.statusCode === 200) {
      res.status(res1.statusCode).json({
        statusCode: res1.statusCode,
        message: res1.message,
        payload: res1.payload
      })
    } else {
      res.status(res1.statusCode).json({
        statusCode: res1.statusCode,
        error: res1.error
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
}