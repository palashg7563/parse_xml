import mongoose, { Schema } from "mongoose";

const DataSchema = new Schema({
  vehicles: [{
    type: Schema.Types.ObjectId
  }],
}, { timestamps: true });


export default mongoose.model("vehicles", DataSchema);