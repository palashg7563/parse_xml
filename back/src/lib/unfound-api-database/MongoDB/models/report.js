import mongoose, { Schema } from "mongoose";

const DataSchema = new Schema({
  id: String,
}, { timestamps: true });


export default mongoose.model("report", DataSchema);