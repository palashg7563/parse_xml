//this file will contain the database function for the report

import Report from "./models/report";

/**
 * @async
 * @function save
 * @description This database function will save the the id for the report generation
 * @author Palash
 * @param {String} id
 * @returns Promise
 * @example
 *
 */
export async function save({ id }) {
  try {
    const report = await Report.create({
      id: id
    })

    return {
      statusCode: 201,
      message: "Success",
      payload: report._id
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// save({ id: "asd" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));


/**
 * @async
 * @function get
 * @description This function is used to get the report by it's id
 * @param {String} id
 * @returns Promise
 * @example
 * get({ id: "asd" })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e))
 */
export async function get({ id }) {
  try {
    const report = await Report.findById(id);

    if (!report) {
      return {
        statusCode: 404,
        error: "Not Found"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: report
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// get({ id: "asd" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));


/**
 * @async
 * @function getAll
 * @description This function is used to get all the report
 * @param {String} id
 * @returns Promise
 * @example
 */
export async function getAll() {
  try {
    const res = await Report.find({});

    return {
      statusCode: 200,
      message: "Success",
      payload: res
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// getAll()
//   .then(e => console.log(e))
//   .catch(e => console.log(e))