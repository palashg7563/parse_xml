//Database function

import json from './models/vehicle';
import Vehicles from "./models/vehicles";

/**
 * This function is used to save the vehicle to the database
 * @async
 * @function savejson
 * @author Palash
 * @description save the json
 * @param {String} id
 * @param {String} frame
 * @param {Array} wheel
 * @param {String} powertrain
 * @returns Promise
 * @example
 * savejson({
 * id: 123,
 * frame: "palash",
 * wheel: "palsad",
 * powertrain: "asd"
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e))
 */
export async function savejson({ id, frame, wheel, powertrain }) {
  try {
    const data = await json.create({
      id: id,
      frame: frame,
      wheels: wheel,
      powertrain: powertrain
    });

    return {
      statusCode: 201,
      message: "Success",
      payload: data._id
    }
  } catch (e) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// 5bf10ead9594101e70c64f6c

// savejson({
//   id: 123,
//   frame: "palash",
//   wheel: "palsad",
//   powertrain: "asd"
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e))

/**
 * @async
 * @function get
 * @description get the the vehicles by id
 * @author Palash
 * @param {String} id
 * @returns Promise
 * @example
 * get({ id: "5bf10ead9594101e70c64f6c" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e))
 */
export async function get({ id }) {
  try {
    const data = await json.findById(id);

    if (!data) {
      return {
        statusCode: 404,
        error: "Not Found"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: data
    }
  } catch (e) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// get({ id: "5bf10ead9594101e70c64f6c" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e))


/**
 * @async
 * @description This function will add the array of vehicle's id
 * @author Palash
 * @function addvehicle
 * @param {Array} id
 * @returns Promise
 * @example
 * addvehicle({ id: ["5bf10bb49e3ae61c285cc394", "5bf1034ff0cca33354aaffa3"] })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 *
 */
export async function addvehicle({ id }) {
  try {
    const data = await Vehicles.create({
      vehicles: [...id]
    })

    return {
      statusCode: 201,
      message: "Success",
      payload: data._id
    }
  } catch (e) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// addvehicle({ id: ["5bf10bb49e3ae61c285cc394", "5bf1034ff0cca33354aaffa3"] })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @async
 * @description This function retreive the vehicles
 * @author Palash
 * @function getvehicles
 * @param {String} id
 * @returns Promise
 * @example
 * getvehicles({ id: "5bf167fb5c0e8f31ccb91d11" })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function getvehicles({ id }) {
  try {
    const vehicles = await Vehicles.findById(id);

    if (!vehicles) {
      return {
        statusCode: 404,
        error: "Not Found"
      }
    }

    const vehicle = [];

    for (let i = 0; i < vehicles.vehicles.length; i++) {
      const res = await get({ id: vehicles.vehicles[i] });
      vehicle.push(res);
    }

    return {
      statusCode: 200,
      message: "Success",
      id: vehicles._id,
      payload: vehicle.map(e => e.payload)
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// getvehicles({ id: "5bf167fb5c0e8f31ccb91d11" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));