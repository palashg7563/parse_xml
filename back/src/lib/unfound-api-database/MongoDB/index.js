import mongoose from 'mongoose';

//setting the Promise of the global promise
mongoose.Promise = global.Promise;

//debugging is on for only for the development process
mongoose.set('debug', true);

try {
  mongoose.connect("mongodb://localhost:27017/unfound", {
    useNewUrlParser: true,
  });
} catch (error) {
  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
  });
}

mongoose.connection
  .once('open', () => console.log('MongoDB is running'))
  .on('error', (e) => { throw e; });
