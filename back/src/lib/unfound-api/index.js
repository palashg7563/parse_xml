import UploadApi from '../unfound-api-upload/route';
import ReportApi from "../unfound-api-report/route";

export default (app) => {
  //All the api are imported here
  app.use("/api/upload", UploadApi);
  app.use("/api/report", ReportApi);

  app.use(function (req, res, next) {
    res.status(404).json({
      statusCode: 404,
      error: "Route Does not found"
    });
  });
}