import React, { Component } from "react";

import style from './style';

//all the components are imported here
import withStyles from "@material-ui/core/styles/withStyles"
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CircularProgress from '@material-ui/core/CircularProgress';

//all the api are imported here
import { upload } from '../../api/json/uploadjson'
import { analyse } from "../../api/json/analysejson";
import { getvehicles } from "../../api/json/getvehicles";
import { save } from "../../api/report/save";

class Home extends Component {
  constructor(props) {
    super(props);
    this.FileUpload = this.FileUpload.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.Analyse = this.Analyse.bind(this);
    this.save = this.save.bind(this);

    this.state = {
      file: null,
      open: false,
      status: null,
      analyse: false,
      filename: null,
      vehicles: {},
      vehicleid: null
    }
  }

  save = async (e) => {
    try {
      const { filename } = this.state;
      const response = await save({ id: filename });

      if (response.res.data.statusCode === 201) {
        this.setState({
          status: "Success",
          open: true
        })
      } else {
        this.setState({
          status: "Error",
          open: true
        })
      }

    } catch (error) {
      console.error(error);
    }
  }

  Analyse = async (e) => {
    this.setState({
      analyse: true
    })
    const { filename } = this.state;
    const response = await analyse({ filename: filename });
    if (response.res.status === 200) {

      const res = await getvehicles({ id: response.res.data.payload.vehicleId });

      const array = {};

      for (let i = 0; i < res.response.payload.length; i++) {
        array[`vehicle_${i}`] = res.response.payload[i]
      }

      this.setState({
        vehicles: array,
        analyse: false,
        vehicleid: res.response.id
      });

    } else {
      this.setState({
        vehicleId: "Error"
      })
    }
  }

  FileUpload = (e) => {
    this.setState({
      file: e.target.files[0]
    });
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ open: false });
  };

  onSubmit = async (e) => {
    try {
      const res = await upload({ file: this.state.file });
      if (res.res.status === 200) {
        this.setState({
          status: "Success",
          open: true,
          filename: res.res.data.payload.filename
        })
      } else {
        this.setState({
          status: "Error",
          open: true
        })
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card className={classes.card}>
          <CardActionArea>
            <CardContent>
              <Typography
                variant="h5"
                color="textPrimary"
              >
                Upload the xml
              </Typography>
              <input
                id="flat-button-file"
                style={{ display: "none" }}
                name="selectedFile"
                type="file"
                onChange={this.FileUpload}
              />
              <label htmlFor="flat-button-file">
                <Button
                  variant="contained"
                  color="primary"
                  size="medium"
                  component="span"
                  className={classes.button}
                >
                  Select File
              </Button>
              </label>
              <Button
                variant="contained"
                color="primary"
                size="medium"
                component="span"
                className={classes.submit}
                onClick={this.onSubmit}
              >
                Upload
            </Button>
              {this.state.file === null ? null : (<Typography>{this.state.file.name}</Typography>)}
            </CardContent>
          </CardActionArea>
        </Card>

        {this.state.status === "Success" ? (<Button
          color="primary"
          variant="text"
          className={classes.analyse}
          onClick={this.Analyse}
        >
          Analyse the XML
        </Button>) : null}

        {this.state.vehicles.length === 0 ? null : (
          <Typography
            variant="h5"
            color="textSecondary"
          >
            {this.state.vehicles[0]}
          </Typography>
        )}

        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{this.state.status}</span>}
          action={[
            <Button key="undo" color="secondary" size="small" onClick={this.handleClose}>
              UNDO
            </Button>,
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
        <div>
          {Object.keys(this.state.vehicles).length === 0 ?
            this.state.analyse === false ? null :
              (<CircularProgress className={classes.analyse1} />) : (
              <Card >
                <CardActionArea>
                  <CardContent>
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell>id</TableCell>
                          <TableCell>Frame</TableCell>
                          <TableCell>Power Train</TableCell>
                          <TableCell>Wheels</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {Object.keys(this.state.vehicles).map(e => {
                          return (
                            <TableRow key={e}>
                              <TableCell>{this.state.vehicles[`${e}`].id}</TableCell>
                              <TableCell>{this.state.vehicles[`${e}`].frame}</TableCell>
                              <TableCell>{this.state.vehicles[`${e}`].powertrain}</TableCell>
                              <TableCell>{this.state.vehicles[`${e}`].wheels}</TableCell>
                            </TableRow>
                          )
                        })}
                      </TableBody>
                    </Table>
                  </CardContent>
                </CardActionArea>
              </Card>
            )
          }
          {Object.keys(this.state.vehicles).length === 0 ?
            this.state.analyse === false ? null :
              (null) : (
              <Button
                variant="contained"
                color="primary"
                size="medium"
                style={{ marginLeft: "40px", marginTop: "20px" }}
                onClick={this.save}
              >
                Save the report
              </Button>)
          }
        </div>
      </div >
    )
  }
}

export default withStyles(style)(Home);