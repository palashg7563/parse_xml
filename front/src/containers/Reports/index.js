import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles"
import style from "./style";

//all the component are imported here
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import RefreshButton from "@material-ui/icons/RefreshTwoTone"
import ToolTip from "@material-ui/core/Tooltip";

//all the api are imported here
import { getAll } from "../../api/report/getAll";
import { analyse } from "../../api/json/analysejson";
import { getvehicles } from "../../api/json/getvehicles";
import { TableRow } from "@material-ui/core";

class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      report: [],
      tables: []
    };
    this.refresh = this.refresh.bind(this);
  };

  async componentDidMount() {
    const res = await getAll();
    this.setState({
      report: res.res.data.payload.map(e => e.id)
    })
  }

  refresh = async (e) => {
    e.preventDefault();
    // const res = await getAll();
    // this.setState({
    //   report: res.res.data.payload.map(e => e.id)
    // });

    const { report } = this.state;
    Object.keys(report).map(async (e) => {
      const response = await analyse({ filename: report[e] });
      if (response.res.status === 200) {
        const res = await getvehicles({ id: response.res.data.payload.vehicleId });
        const array = {};
        for (let i = 0; i < res.response.payload.length; i++) {
          array[`vehicle_${i}`] = res.response.payload[i]
        }

        this.setState({
          tables: [...this.state.tables, array]
        })
      }
    })
  }

  render() {
    return (
      <div>
        <div>
          <Typography
            variant="h3"
            color="textPrimary"
            style={{ textAlign: "center" }}
          >
            Reports
            <ToolTip placement="right" title="Refresh">
              <IconButton onClick={this.refresh}>
                <RefreshButton />
              </IconButton>
            </ToolTip>
          </Typography>
        </div>
        <div>
          {this.state.report.length === 0 ?
            (<Typography
              color="primary"
              variant="h4"
              style={{ textAlign: "center", marginTop: "40px" }}
            >
              No Report To Display
            </Typography>) :
            ((Object.keys(this.state.tables).map(e => this.state.tables[e]).map(f => {
              return (
                <Card style={{ marginTop: "20px" }}>
                  <CardActionArea>
                    <CardContent>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell>id</TableCell>
                            <TableCell>Frame</TableCell>
                            <TableCell>Power Train</TableCell>
                            <TableCell>Wheels</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {Object.keys(f).map(r => {
                            return (
                              <TableRow key={r}>
                                <TableCell>{f[`${r}`].id}</TableCell>
                                <TableCell>{f[`${r}`].frame}</TableCell>
                                <TableCell>{f[`${r}`].powertrain}</TableCell>
                                <TableCell>{f[`${r}`].wheels}</TableCell>
                              </TableRow>
                            )
                          })}
                        </TableBody>
                      </Table>
                    </CardContent>
                  </CardActionArea>
                </Card>
              )
            })))
          }
        </div>
      </div>
    )
  }
}

export default withStyles(style)(Report);