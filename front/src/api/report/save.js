import axios from "axios";

export async function save({ id }) {
  try {
    const url = "http://localhost:3000/api/report/save";

    const res = await axios.post(url, {
      id: id
    });

    return {
      res
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Something went wrong"
    }
  }
}