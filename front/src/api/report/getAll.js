import axios from "axios";

export async function getAll() {
  try {
    const url = "http://localhost:3000/api/report/";

    const res = await axios.post(url)

    return {
      res
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Something went wrong"
    }
  }
}

// getAll()
//   .then(e => console.log(e))
//   .catch(e => console.log(e));