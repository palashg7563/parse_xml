import axios from "axios";


export async function analyse({ filename }) {
  try {
    const url = `http://localhost:3000/api/upload/json`;

    const res = await axios.post(url, {
      filename: filename
    });

    const { statusCode } = res;

    if (statusCode >= 300) {
      return {
        statusCode: statusCode,
        error: "Error"
      }
    }

    return {
      res
    }

  } catch (e) {
    return {
      statusCode: 500,
      error: "Something went wrong"
    }
  }
}