import axios from "axios";

export async function getvehicles({ id }) {
  try {
    const url = `http://localhost:3000/api/upload/vehicles`;

    const resposne = await axios.post(url, {
      id
    });

    if (resposne.data.statusCode >= 300) {
      return {
        statusCode: resposne.data.statusCode,
        error: resposne.data.error
      }
    } else {
      return {
        response: resposne.data
      }
    }

  } catch (error) {
    return {
      statusCode: 500,
      error: "Something went wrong"
    }
  }
}