import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { theme } from "./theme";
import Route from "react-router-dom/Route";
import Switch from "react-router-dom/Switch";
import BrowserRouter from "react-router-dom/BrowserRouter";

//All the container are imported here
import AppLayout from "./containers/AppLayout";
import Home from "./containers/Home";
import Reports from "./containers/Reports";

//All the components are imported here
import NotFound from "./components/NotFound";


class App extends Component {
  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
          <AppLayout>
            <BrowserRouter>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/reports" component={Reports} />
                <Route component={NotFound} />
              </Switch>
            </BrowserRouter>
          </AppLayout>
        </MuiThemeProvider>
      </React.Fragment>
    )
  }
}

export default App;