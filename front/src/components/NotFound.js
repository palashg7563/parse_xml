import React, { Component } from "react";
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Error from "@material-ui/icons/Error";
import withRouter from "react-router-dom/withRouter"

const styles = (theme) => ({
  main: {
    // backgroundColor: "red",
  },
  text: {
    textAlign: "center",
    margin: "30px",
    fontSize: "100px"
  },
  subtext: {
    textAlign: "center",
    fontSize: "50px"
  },
  buttons: {
    textAlign: "center",
    marginTop: "10px"
  }
});

class NotFound extends Component {
  onClick = () => {
    const { history } = this.props;
    history.push("/");
  }
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.text}>
          <Typography variant="display4">
            4 0 4<Error style={{ paddingTop: "10px" }} color="secondary" fontSize="inherit" />
          </Typography>
        </div>
        <div>
          <Typography className={classes.buttons} variant="h5" color="primary">
            want to go?
          </Typography>
        </div>
        <div className={classes.buttons}>
          <Button variant="outlined" onClick={this.onClick}> Home</Button>
        </div>
      </div>)
  }
}

export default withRouter(withStyles(styles)(NotFound));